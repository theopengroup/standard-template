include::_strings.adoc[]

= {tog-doctitle} : {tog-producer} {tog-doctype}
:id:
:doctype:   book
:doctitle: {tog-doctitle} : +
{tog-producer} {tog-doctype}
ifeval::["{tog-draft}" == "DRAFT"]
Unapproved Draft, Subject to Change
The Open Group Confidential
endif::[]
// Uncomment next line to set page size (default is Letter for standards theme)
// for example for a handbook, set A5
//:pdf-page-size: A5
:producer: {tog-producer}
:keywords: {tog-keywords}
:copyright: ©{tog-producer}, {tog-copyright-year}
:docinfo:
:toc: left
:toclevels: 4
:sectnumlevels: 4
//:front-cover-image: {tog-coverimage}
:linkattrs:
:sectnums!:
:bibtex-file: references.bib
:bibtex-style: ieee
//:bibtex-style: harvard1
:bibtex-order: alphabetical
// this is intended to be run with asciidoctor-pdf
// attribute below is for including/suppressing pdf specific things, check with "ifdef"
:adoc-pdf:
:pdf-themesdir: resources/pdf-themes
:pdf-theme: standards-theme.yml
ifeval::["{tog-hardcopy}" == "true"]
:pdf-theme: standards-theme-hardcopy.yml
endif::[]
ifeval::["{tog-osdu}" == "true"]
:pdf-theme: standards-theme-osdu.yml
endif::[]
// commentary for instructors and trainers is left in for now.
:instructor-ed:
// this does not work
//:imagesdir: images
:leveloffset: -1
// set full xrefs so they contain figure/table name etc.
//:xrefstyle: full
:xrefstyle: short
include::_files.adoc[]
