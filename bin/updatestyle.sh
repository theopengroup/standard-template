#!/bin/sh
# update html stylesheet to use corporate color #00667F
# called by render-html.sh

if [ "$TOG_DOCNAME" = "" ]
then
   . _vars.sh
   echo "TOG_DOCNAME not set in environment, using setting from _vars.sh"
fi
export TOG_DOCNAME

cd output/html && cat ${TOG_DOCNAME}.html | sed -e '/<style>/,/<\/style>/s/#ba3925/#00667F/g' \
    -e '/<style>/,/<\/style>/s/#7a2518/#00667F/g' \
    -e '/<style>/,/<\/style>/s/#ba3925/#00667F/g' > tmp

if [ $? -eq 0 ]
then
	mv tmp ${TOG_DOCNAME}.html
fi
