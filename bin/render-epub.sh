#!/bin/bash
# simple epub file build - independent

if [ ! -r bin/sedscript_file ]
then
    echo "$0: prerequisite bin/sedscript_file not found"
    exit 1
fi

rm -rf images
bin/image-cache.sh

# note this shares the same html top level spine file
TOG_DOCNAME=$1

echo "Preprocess"
asciidoctor-reducer _book-html.adoc >output/preproc.adoc
echo "Adjust source"
cat output/preproc.adoc |sed -f bin/sedscript_file >_pout.adoc
echo "Processing epub"
asciidoctor-epub3 -v -r asciidoctor-diagram -D output/epub -o ${TOG_DOCNAME}.epub _pout.adoc
echo "Processing complete"

# remove images
rm -rf images
