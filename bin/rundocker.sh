#!/bin/sh

#Invoke the build environment in a docker container

# Prerequites: Docker installed
# Should be invoked from the top level directory of the document as
#      bin/rundocker.sh

command -v docker >/dev/null 2>&1 || { echo >&2 "Prequisite: docker not found.  Aborting."; exit 1; }

if [ ! -r bin/adocbuild.sh ]
then
   echo "Unable to locate build scripts, check current directory. Aborting"; exit 1
fi

#docker run -it -v `pwd`:/documents opengroup/og-asciidoc-build-tools:2024-1
docker run -it -v `pwd`:/documents opengroup/digital-portfolio-build-tools:latest
