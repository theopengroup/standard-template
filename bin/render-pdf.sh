#!/bin/bash
# simple PDF file build - independent


if [ ! -r bin/sedscript_file ]
then
    echo "$0: prerequisite bin/sedscript_file not found"
    exit 1
fi

# setup images at top level for inclusion in builds
rm -rf images
bin/image-cache.sh

TOG_DOCNAME=$1

echo "Preprocess"
asciidoctor-reducer  _book-pdf.adoc >output/preproc.adoc
echo "Adjust source"
cat output/preproc.adoc |sed -f bin/sedscript_file >_pout.adoc
echo "Processing pdf"
asciidoctor-pdf -v -r asciidoctor-diagram -D output/pdf -o ${TOG_DOCNAME}.pdf _pout.adoc
echo "Processing complete"

# remove images
rm -rf images
