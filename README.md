# The Open Group Standard Template

A template for developing standards and guides for The Open Group written in AsciiDoc using the Asciidoctor toolchain.
This version is configured to auto build the html and pdf output on The Open Group GitLab server. It can also be built locally which is useful for development purposes. This version now supports Antora module development, with the file tree and basic syntax defaulting to Antora compatible, and the build scripts adjusting for single book format.

You can view the latest build of this template here: http://standards-process.projects.opengroup.org/the-open-group-standard-template.

*Please note that users are expected to be proficient in the use of GitLab and Asciidoctor.
This is not intended as a tutorial for configuring Git clients, nor installing the toolchain. For a first introduction there is a separate handbook available.
For example to build the Antora output you need to set the GIT_CREDENTIALS ci/cd variable if running on a protected project.*

This template can be configured to build a number of document types including:

* Standard
* Snapshot of a standard
* Guide
* TOGAF Series Guide
* Standard developed by the OSDU Forum
* Guide developed by the OSDU Forum

The selection of whether to build a standard, snapshot of a standard, or a guide, etc. is achieved by setting configuration variables in the \_strings.adoc file.

A separate template is available for white papers here: https://gitlab.opengroup.org/standards-process/the-open-group-white-paper-template.

The latest update to the toolchain is inclusion of support for Asciidoctor Diagram, a set of Asciidoctor extensions that enable you to add diagrams, which you describe using plain text, to your AsciiDoc document.
The extensions support the AsciiToSVG, BlockDiag (BlockDiag, SeqDiag, ActDiag, NwDiag), Ditaa, Erd, GraphViz, Mermaid, Msc, PlantUML, Shaape, SvgBob, Syntrax, UMLet, Vega, Vega-Lite, and WaveDrom syntax.
For more details, see https://asciidoctor.org/docs/asciidoctor-diagram/. Note we have tested use of the template with Ditaa and PlantUML support.

Note that a more detailed description is contained in Chapter 4 of the document template when built.

The file tree is organized as follows:

*  bin (directory) - scripts to build the document in a number of formats
*  resources (directory) - style sheets for building pdf
*  modules (directory) - the actual document text
*  output (directory) - where output files are created

In general, files beginning underscore (\_) are configuration files. They may need to be tailored for the document.

Configuration files:

*  \_strings.adoc - document strings to configure the document
*  \_files.adoc - the table of contents for the document layout
*  \_vars.sh - variables in shell script syntax to configure the build scripts
*  \_book-html.adoc - configuration for the html book output
*  \_book-pdf.adoc - configuration for the pdf book output
*  \antora.yml - top level Antora configuration file

Working with the template:

* Review \_strings.adoc and edit the variables
* Building the document locally using bin/adocbuild.sh

The builds can be run using the automated pipeline build or run locally on the client using scripts in the build directory.

To build the document locally, use the script in the _bin_ directory:

  $ bin/adocbuild.sh

The options are:

  bin/adocbuild.sh [clean | html | pdf | all]

Ruby Prerequisites: asciidoctor, (asciidoctor-bibtex), asciidoctor-pdf, asciidoctor-diagram unicode_utils

To understand the template it is best to build the pdf document, and read it.
To build an Antora site, typically a separate project is setup with an antora-playbook.yml file that pulls in the project and generates a site.
