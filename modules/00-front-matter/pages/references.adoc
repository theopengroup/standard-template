//Referenced Documents should be listed here.
//Ideally, each reference should include the title, author, date, publisher, and document number.
//Where available, add a URL so that the reader can access the document online.
//This section should be subdivided into Normative References and Informative References using the Unnumbered Heading style.
//For standards intended to be adopted by ISO, the Normative References should be a forward reference to Chapter 1;
//for example:
//Normative references for POSIX.1-2008 are defined in Section 1.4.

[#Referenced-docs]
== Referenced Documents

ifeval::["{tog-guidance}" == "true"]
******************
*Guidance*

Referenced Documents should be listed here.

Ideally, each reference should include the title, author, date, publisher, and document number.

Where available, add a URL so that the reader can access the document online.

ifeval::["{tog-doctype}" != "Guide"]
// The following for standards only
This section should be subdivided into Normative References and Informative References using the Unnumbered Heading style.

For standards intended to be adopted by ISO, the Normative References should be a forward reference to Chapter 1;
for example:

Normative references for POSIX.1-2008 are defined in Section 1.4.
endif::[]

You may choose to use a tool to generate a Bibliography (see the references.bib file).

AsciiDoc also has basic support for a Bibliography; however, that needs the references to be placed at the end of the document. We do not further describe that here.

******************
endif::[]

ifeval::["{tog-doctype}" != "Guide"]
The following documents are referenced in this {tog-doctype}.

(Please note that the links below are good at the time of writing but cannot be guaranteed for the future.)

[#NormativeReferences]
=== Normative References

This document does not contain any normative references at the time of publication. These may be added in a future release.

[#InformativeReferences]
=== Informative References
endif::[]

ifeval::["{tog-doctype}" == "Guide"]

The following documents are referenced in this Guide.

(Please note that the links below are good at the time of writing but cannot be guaranteed for the future.)

endif::[]
// If you are using a bibtex Bibliography use the format below:
//bibliography::[]
//else
// use a frameless table

//See below for the structure for Referenced Documents to be listed with tags that can be used as bookmarks to link back from the text directly to the full reference; for example, "`as defined in the TOGAF Standard [xref:C220[C220]].`"
// for Antora documents xref format when outside the same file is extended
// fpr example [xref:00-front-matter:references.doc#C220[C220]]

[frame=none,grid=none,cols="1,13"]
|===
|[[Tag]][Tag] |Title, Author, Document Number, Publisher, Date, Document Number; refer to: URL
|[[C220]][C220] |The TOGAF^(R)^ Standard, 10^th^ Edition, a standard of The Open Group (C220), published by The Open Group, April 2022; refer to: http://www.opengroup.org/library/c220[www.opengroup.org/library/c220]
|[[Ref1]][Ref1]| The Open Group Standards Process; refer to https://www.opengroup.org/standardsprocess[www.opengroup.org/standardsprocess]
|[[Tag2]][Tag2] |Etc.
|===
