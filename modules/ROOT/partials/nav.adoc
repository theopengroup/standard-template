.The Open Group Standard Template
* Front Matter
** xref:the-open-group-standard-template:00-front-matter:title-page.adoc[Title]
** xref:the-open-group-standard-template:00-front-matter:copyright.adoc[Copyright]
** xref:the-open-group-standard-template:00-front-matter:preface.adoc[Preface]
** xref:the-open-group-standard-template:00-front-matter:trademarks.adoc[Trademarks]
** xref:the-open-group-standard-template:00-front-matter:acknowledgements.adoc[Acknowledgements]
** xref:the-open-group-standard-template:00-front-matter:references.adoc[Referenced Documents]
* xref:the-open-group-standard-template:chap01.adoc[Introduction]
* xref:the-open-group-standard-template:chap02.adoc[Definitions]
* xref:the-open-group-standard-template:chap03.adoc[Sample Heading at Level 1]
* xref:the-open-group-standard-template:part1.adoc[Part Title]
* xref:the-open-group-standard-template:chap04.adoc[Using the AsciiDoctor Standard Template]
* Appendices
** xref:the-open-group-standard-template:99-appendices:section-intro.adoc[Appendices]
** xref:the-open-group-standard-template:99-appendices:appendix.adoc[Appendix A]
** xref:the-open-group-standard-template:99-appendices:rationale.adoc[Rationale]
